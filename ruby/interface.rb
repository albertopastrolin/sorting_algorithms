require "benchmark"
require "./define.rb"
require "./array.rb"
require "./heap.rb"
require "./inserction_sort.rb"
require "./merge_sort.rb"
require "./quick_sort.rb"
require "./heap_sort.rb"
require "./counting_sort.rb"
require "./radix_sort.rb"
require "./bucket_sort.rb"

def menu()
  begin
    puts "0) Back"
    puts "1) chain_hash_function"
    puts "2) open_hash_function"
    choice = gets.to_i
    case choice
      when 1
        chain_hash_function()
      when 2
        open_hash_function()
      when 0
        puts "Exit..."
      else
        puts "Unsupported Option"
        redo
    end
  end while choice > 0
end

def array_function()
begin
    puts "0) Back"
    puts "1) comparison_sort"
    puts "2) non_comparison_sort"
    puts "3) other_sort"
    choice = gets.to_i
    case choice
      when 1
        comparison_sort()
      when 2
        non_comparison_sort()
      when 3
        other_sort()
      when 0
        puts "Back..."
      else
        puts "Unsupported Option"
        redo
    end
  end while choice > 0
end

def comparison_sort()
  array = Array.new(LENGTH)
  begin
    puts "0) Back"
    puts "1) array_initialize"
    puts "2) array_print"
    puts "3) array_reverse"
    puts "4) insection_sort"
        puts "5) merge_sort"
        puts "6) quick_sort"
        puts "7) naive_quick_sort"
    choice = gets.to_i
    case choice
      when 1
        array_initialize(array)
      when 2
        p array
      when 3
        array.reverse!
      when 4
        puts Benchmark.realtime() { inserction_sort(array) }
      when 5
        puts Benchmark.realtime() { merge_sort(array, 0, LENGTH - 1) }
      when 6
        puts Benchmark.realtime() { quick_sort(array, 0, LENGTH - 1) }
      when 7
        puts Benchmark.realtime() { naive_quick_sort(array, 0, LENGTH - 1) }
      when 0
        puts "Back..."
      else
        puts "Unsupported Option"
        redo
    end
  end while choice > 0
end

def non_comparison_sort()
  array = Array.new(LENGTH)
  begin
    puts "0) back"
    puts "1) array_initialize"
        puts "2) array_print"
        puts "3) array_reverse"
        puts "4) counting_sort"
        puts "5) radix_sort"
        puts "6) bucket_sort"
    choice = gets.to_i
    case choice
      when 1
        array_initialize(array)
      when 2
        p array
      when 3
        array.reverse!
      when 4
        puts Benchmark.realtime() { array = counting_sort(array, MAX_RAND) }
      when 5
        puts Benchmark.realtime() { array = radix_sort(array) }
      when 6
        puts Benchmark.realtime() { bucket_sort(array) }
      when 0
        puts "Back..."
      else
        puts "Unsupported Option"
        redo
    end
  end while choice > 0
end

def other_sort()
  heap = Array.new(LENGTH + 1)
  begin
    puts "0) back"
    puts "1) heap_initialize"
        puts "2) heap_print"
        puts "3) heap_sort"
    choice = gets.to_i
    case choice
      when 0
        puts "Back..."
      when 1
        heap_initialize(heap)
      when 2
        heap_print(heap)
      when 3
        puts Benchmark.realtime() { heap_sort(heap) }
      else
        puts "Unsupported Option"
        redo
    end
  end while choice > 0
end
  begin
    puts "0) Back"
    puts "1) chain_hash_initialize"
    puts "2) chain_hash_print"
    puts "3) chain_hash_insert"
    puts "4) chain_hash_delete"
    puts "5) chain_hash_search"
    choice = gets.to_i
    case choice
      when 1
        chain_hash = Array.new(TABLE_LENGTH) {Array.new()}
      when 2
        p chain_hash
      when 3
        puts "Insert a value"
        value = gets.to_i
        chain_hash_insert(chain_hash, value)
      when 4
        puts "Insert a value"
        value = gets.to_i
        chain_hash_delete(chain_hash, value)
      when 5
        puts "Insert a value"
        value = gets.to_i
        index = chain_hash_search(chain_hash, value)
        puts "Finded " + value.to_s + " at location "+ index.to_s + " of the " + (hash(value) + 1).to_s + " list"
      when 0
        puts "Back..."
      else
        puts "Unsupported Option"
        redo
    end
  end while choice > 0
end

def open_hash_function()	
  begin
    puts "0) Back"
    puts "1) open_hash_initialize"
    puts "2) open_hash_print"
    puts "3) open_hash_insert"
    puts "4) open_hash_delete"
    puts "5) open_hash_search"
    choice = gets.to_i
    case choice
      when 1
        open_hash = Array.new(TABLE_LENGTH, "VOID")
      when 2
        p open_hash
      when 3
        puts "Insert a value"
        value = gets.to_i
        open_hash_insert(open_hash, value)
      when 4
        puts "Insert a value"
        value = gets.to_i
        open_hash_delete(open_hash, value)
      when 5
        puts "Insert a value"
        value = gets.to_i
        index = open_hash_search(open_hash, value)
        puts "Finded " + value.to_s + " at location "+ index.to_s
      when 0
        puts "Back..."
      else
        puts "Unsupported Option"
        redo
    end
  end while choice > 0
end

menu()
