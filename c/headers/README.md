Description of the Various Headers
==================================

A quick description of the various header

* array.h  
  In this header you can find various functions to manipulate an array
  
* define.h  
  In this header you can find the definition of various macro

* heap.h  
  In this header you can find an implentation of a max heap, and some  
  functions to manipulate it
  
* heap_sort.h  
  In this header you can find an implementation of the heap sort algorithm
  
* inserction_sort.h  
  In this header you can find an implementation of the inserction sort algorithm

* merge_sort.h  
  In this header you can find an implementation of the merge sort algorithm

* quick_sort.h  
  In this header you can find an implementation of the quick sort algorithm
  
* counting_sort.h  
  In this header you can find an implementation of the counting sort algorithm
  
* radix_sort.h  
  In this header you can find an implementation of the radix sort algorithm

* bucket_sort.h   
  In this header you can find an implementation of the bucket sort algorithm
