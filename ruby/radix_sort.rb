def radix_sort (inp)
  out = Array.new(LENGTH, 0)
  work = Array.new(BASE, 0)
  digit = 1
  while (MAX_RAND / digit) > 0
    work.fill(0)
    for i in 0 .. (LENGTH - 1)
      work[(inp[i] / digit) % BASE] += 1
    end
    for i in 1 .. (BASE - 1)
      work[i] += work[i - 1]
    end
    for i in (LENGTH - 1).downto(0)
      out[work[(inp[i] / digit) % BASE]] = inp[i]
      work[(inp[i] / digit) % BASE] -= 1
    end
    digit *= BASE
  end
  return out
end
