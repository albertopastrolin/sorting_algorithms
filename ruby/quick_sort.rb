def quick_sort(array, left, right)
    if left < right
        pivot=partition(array, left, right)
        quick_sort(array, left, pivot - 1)
        quick_sort(array, pivot + 1, right)
    end
end

def naive_quick_sort(array, left, right)
  if left < right
    pivot=select_naive(array, left, right, (left + right) / 2)
    quick_sort(array, left, pivot - 1)
    quick_sort(array, pivot + 1, right)
  end
end
