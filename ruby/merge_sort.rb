def merge_sort (array, left, right)
    if left < right
        int center = (left + right) / 2
        merge_sort(array, left, center)
        merge_sort(array, center + 1, right)
        merge(array, left, center, right)
    end
end

def merge (array, left, center, right)
    i = left, j = center + 1, k = left, block = Array.new(LENGTH);
    while i <= center && j <= right
        if array[i] <= array[j]
            block[k] = array[i]
            i += 1
        else
            block[k] = array[j]
            j += 1
        end
        k += 1
    end
    while i <= center 
        block[k] = array[i]
        i += 1
        k += 1
    end
    while j <= right
        block[k] = array[j]
        j += 1
        k += 1
    end
    array = Array.new(block)
end
