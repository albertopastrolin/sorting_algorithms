

/*
 * Various function to manipulate the array;
 * to use it you also need this header:
 * define.h
    * RINGO!*
 */


void array_initialize(int array[]);
void array_fill(int array[], int length, int value);
void switcheroo(int array[], int i, int j);
void clone(int array[], int clo[]);
void array_reverse(int array[]);
int partition(int array[], int left, int right);
int select_naive(int array[], int left, int right, int index);
void array_print(int array[]);

void array_initialize(int array[]){
    srand(time(NULL));
    int i;
    for(i = 0; i < LENGTH; i += 1)
        array[i] = MIN_RAND + (rand() % (MAX_RAND - MIN_RAND + 1));
}

void switcheroo(int array[], int i, int j){
    int tmp = array[i];
    array[i] = array[j];
    array[j] = tmp;
}

void clone(int array[], int clo[]){
  int i;
  for(i = 0; i < LENGTH; i += 1)
    clo[i] = array[i];
}

void array_reverse(int array[]){
  int i = 0, j = LENGTH -1;
  while (i < j){
    switcheroo(array, i, j);
    i += 1;
    j -= 1;
  }
}

int partition(int array[], int left, int right){
    int j, i = left - 1;
    for (j = left; j < right; j += 1)
        if (array[j] <= array[right]){
            i += 1;
            switcheroo (array, i , j);
        }
    i += 1;
    switcheroo (array, right, i);
    return i;
}

int select_naive(int array[], int left, int right, int index){
  if (left < right){
    int ret = partition(array, left, right);
    if (ret == index)
      return array[ret];
    else
      if (index < ret)
        return select_naive(array, left, ret - 1, index);
      else
        return select_naive(array, ret + 1, right, index);
  }
  else
    return array[left];
}

void array_fill(int array[], int length, int value){
  int i;
  for (i = 0; i < length; i += 1)
    array[i] = 0;
}

void array_print(int array[]){
  int i;
  for(i = 0; i < LENGTH; i += 1)
    if((i +1) % ELEMENTS_FOR_ROW)
      if((i + 1) % ELEMENTS_FOR_ROW)
        printf ("%8d", array[i]);
      else
        printf ("%8d\n", array[i]);
      printf("\n");
}