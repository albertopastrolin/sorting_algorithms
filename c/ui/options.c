

/*
 * This is a quick interface for testing purpose
 * I have tested it only under GNU/Linux, but probably it will work
 * on every unix-like system, so under GNU/Linux, BSD and Mac OS X it
 * should work (let me know if not); under Windows probably it won't work,
 * if I remember correctly the time library needs some adjustment, so try it
 * and if it works, ok, else fix it, your problem ^-^.
 * Ah, if you use an "exotic" OS (eg openIndiana), if it is POSIX compliant
 * probably this interface will work, if not it same of Windows, if it works ok,
 * else fix it.
*/


#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "define.h"
#include "array.h"
#include "heap.h"
#include "inserction_sort.h"
#include "merge_sort.h"
#include "quick_sort.h"
#include "heap_sort.h"
#include "counting_sort.h"
#include "radix_sort.h"
#include "bucket_sort.h"


void comparison_sort(int status);
void non_comparison_sort(int status);
void other_sort(int status);
void full_menu();


void comparison_sort(int status){
  int choice, return_value, array[LENGTH];
    long start_time, end_time;
    double work_time;
    do{
        status ? printf ("0) Exit\n") : printf ("0) Back\n");
        printf ("1) array_initialize\n");
        printf ("2) array_print\n");
        printf ("3) array_reverse\n");
        printf ("4) inserction_sort\n");
        printf ("5) inserction_sort_with_count\n");
        printf ("6) merge_sort\n");
        printf ("7) quick_sort\n");
        printf ("8) naive_quick_sort\n");
        scanf ("%d", &choice);
        switch (choice){
            case 1:
                array_initialize(array);
            break;
            case 2:
                array_print(array);
            break;
            case 3:
        array_reverse(array);
      break;
            case 4:
                start_time = clock();
                inserction_sort(array);
                end_time = clock();
                work_time = (double) (end_time - start_time) / CLOCKS_PER_SEC;
                printf ("Sorted in %5.10f seconds\n", work_time);
            break;
            case 5:
                return_value = inserction_sort_with_count(array);
                printf ("Sorted with %d operations\n", return_value);
            break;
            case 6:
                start_time = clock();
                merge_sort(array, 0 , LENGTH - 1);
                end_time = clock();
                work_time = (double) (end_time - start_time) / CLOCKS_PER_SEC;
                printf ("Sorted in %5.10f seconds\n", work_time);
            break;
            case 7:
                start_time = clock();
                quick_sort(array, 0 , LENGTH - 1);
                end_time = clock();
                work_time = (double) (end_time - start_time) / CLOCKS_PER_SEC;
                printf ("Sorted in %5.10f seconds\n", work_time);
            break;
            case 8:
                start_time = clock();
                naive_quick_sort(array, 0 , LENGTH - 1);
                end_time = clock();
                work_time = (double) (end_time - start_time) / CLOCKS_PER_SEC;
                printf ("Sorted in %5.10f seconds\n", work_time);
            break;
        }
    }
    while(choice);
}

void non_comparison_sort(int status){
  int choice, array[LENGTH], clo[LENGTH];
    long start_time, end_time;
    double work_time;
    do{
        status ? printf ("0) Exit\n") : printf ("0) Back\n");
        printf ("1) array_initialize\n");
        printf ("2) array_print\n");
        printf ("3) array_reverse\n");
        printf ("4) counting_sort\n");
        printf ("5) radix_sort\n");
        printf ("6) bucket_sort\n");
        scanf ("%d", &choice);
        switch (choice){
            case 1:
                array_initialize(array);
            break;
            case 2:
                array_print(array);
            break;
            case 3:
        array_reverse(array);
      break;
            case 4:
        clone (array, clo);
        start_time = clock();
                counting_sort(clo, array, MAX_RAND);
                end_time = clock();
                work_time = (double) (end_time - start_time) / CLOCKS_PER_SEC;
                printf ("Sorted in %5.10f seconds\n", work_time);
            break;
            case 5:
        clone (array, clo);
        start_time = clock();
                radix_sort(clo, array);
                end_time = clock();
                work_time = (double) (end_time - start_time) / CLOCKS_PER_SEC;
                printf ("Sorted in %5.10f seconds\n", work_time);
            break;
            case 6:
                start_time = clock();
                bucket_sort(array);
                end_time = clock();
                work_time = (double) (end_time - start_time) / CLOCKS_PER_SEC;
                printf ("Sorted in %5.10f seconds\n", work_time);
            break;
        }
    }
    while(choice);
}

void other_sort(int status){
  int choice, heap[LENGTH + 1];
    long start_time, end_time;
    double work_time;
    do{
        status ? printf ("0) Exit\n") : printf ("0) Back\n");
        printf ("1) heap_initialize\n");
        printf ("2) heap_print\n");
        printf ("3) heap_sort\n");
        scanf ("%d", &choice);
        switch (choice){
            case 1:
                heap_initialize(heap);
            break;
            case 2:
                heap_print(heap);
            break;
            case 3:
                start_time = clock();
                heap_sort(heap);
                end_time = clock();
                work_time = (double) (end_time - start_time) / CLOCKS_PER_SEC;
                printf ("Sorted in %5.10f seconds\n", work_time);
            break;
        }
    }
    while(choice);
}

void full_menu(){
  int choice;
  do{
    printf ("0) Exit\n");
    printf ("1) comparison_sort\n");
    printf ("2) non_comparison_sort\n");
    printf ("3) other_sort\n");
    scanf ("%d", &choice);
    switch(choice){
      case 1:
        comparison_sort(0);
        break;
      case 2:
        non_comparison_sort(0);
        break;
      case 3:
        other_sort(0);
        break;
    }
  }
  while(choice);
}