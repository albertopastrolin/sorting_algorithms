def heapsize(heap)
    return heap[0]
end

def increase(heap)
    heap[0] += 1
end

def decrease(heap)
    heap[0] -= 1
end

def setsize(heap, size)
    heap[0] = size
end

def left(i)
    return 2 * i
end

def right(i)
    return (2 * i) - 1
end

def heapify(heap, error)
    if left(error) <= heapsize(heap) && heap[left(error)] > heap[error]
        m = left(error)
    else
        m = error
    end
    if  right(error) <= heapsize(heap) && heap[right(error)] > heap[m]
        m = right(error)
    end
    if m != error
        heap[m], heap[error] = heap[error], heap[m]
        heapify(heap, m)
    end
end

def extract_max(heap)
    max = heap[1]
    heap[1] = heap[heapsize(heap)]
    decrease(heap)
    heapify(heap, 1)
    return max
end

def build_heap(array)
    setsize(array, LENGTH)
    for i in (LENGTH / 2).downto(1)
        heapify(array, i)
    end
end

def heap_initialize(heap)
    array_initialize(heap)
    heap[0], heap[LENGTH] = heap[LENGTH], heap[0]
    build_heap(heap)
end

def heap_print(heap)
  puts "Heapsize: ", heap[0]
  print heap[1..LENGTH], "\n"
end
