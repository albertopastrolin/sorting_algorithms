def array_initialize(array)
  for i in 0 .. (array.length - 1)
    array[i] = MIN_RAND + Random.rand(MAX_RAND - MIN_RAND + 1)
  end
end

def partition(array, left, right)
    i = left - 1
    for j in left .. (right - 1)
        if array[j] <= array[right]
            i += 1;
            array[i], array[j] = array[j], array[i]
        end
    end
    i += 1
    array[i], array[right] = array[right], array[i]
    return i
end

def select_naive(array, left, right, index)
  if left < right
    ret = partition(array, left, right)
    if ret == index
      return array[ret]
    else
      if index < ret
        return select_naive(array, left, ret - 1, index)	
      else
        return select_naive(array, ret + 1, right, index)
      end
    end
  else
    return array[left]
  end
end
