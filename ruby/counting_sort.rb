def counting_sort (inp, k)
  out = Array.new(LENGTH, 0)
  work = Array.new(k+1, 0)
  for i in 0 .. (LENGTH - 1)
    work[inp[i]] += 1
  end
  for i in 1 .. k
    work[i] += work[i - 1]
  end
  for i in (LENGTH - 1).downto(0)
    out[work[inp[i]]] = inp[i]
    work[inp[i]] -= 1
  end
  return out
end
