

/*
 * This is an implementation of the insertion sort:
 * you can find an implementation that simply is the conversion from the pseudo code,
 * and one implementation that also count the operations.
 * To use it you also need this other header:
 * define.h
 * array.h
*/


void inserction_sort(int array[]);
int inserction_sort_with_count(int array[]);


void inserction_sort(int array[]){
    int i, j, key;
    for (j = 1; j < LENGTH; j += 1){
        key = array[j];
        i = j - 1;
        while (array[i] > key && i >= 0){
            array[i+1] = array[i];
            i -= 1;
        }
        array[i+1] = key;
    }
}

int inserction_sort_with_count(int array[]){
    int i, j, key, count = LENGTH;
    for (j = 1; j < LENGTH; j += 1){
        key = array[j];
        i = j - 1;
        while ((count+=1 || count) && array[i] > key && i >= 0){
            array[i+1] = array[i];
            i -= 1;
            count += 2;
        }
        array[i+1] = key;
        count += 3;
    }
    return count;
}
