sorting_algorithms
==================

A collection of sorting algorithms implemented in standard C

To compile, simply use the "make" utility:

* make  
  To compile everything

* make full  
  To compile an interactive UI with every options

* make comp  
  To compile an interactive UI only for the comparative sorting algorithms

* make ncomp  
  To compile an interactive UI only for the non comparative sorting algorithms

* make oth  
  To compile an interactive UI only for the heap sort

* make clean  
  To delete any executable/leftovers file
