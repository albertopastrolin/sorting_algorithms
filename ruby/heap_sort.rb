def heap_sort (array)
    build_heap(array)
    for i in LENGTH.downto(2)
        array[1], array[i] = array[i], array[1]
        decrease(array)
        heapify(array, 1)
    end
end
