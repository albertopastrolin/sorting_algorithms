

/*
 * This is an implementation of the heap sort;
 * to use it you also need this header:
 * define.h
 * array.h
 * heap.h
*/


void heap_sort(int array[]);


void heap_sort (int array[]){
    int i;
    build_heap(array);
    for (i = LENGTH; i >= 2; i -= 1){
        switcheroo(array, 1, i);
        decrease(array);
        heapify(array, 1);
    }
}
