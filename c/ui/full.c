

/*
 * This is a quick interface for testing purpose
 * I have tested it only under GNU/Linux, but probably it will work
 * on every unix-like system, so under GNU/Linux, BSD and Mac OS X it
 * should work (let me know if not); under Windows probably it won't work,
 * if I remember correctly the time library needs some adjustment, so try it
 * and if it works, ok, else fix it, your problem ^-^.
 * Ah, if you use an "exotic" OS (eg openIndiana), if it is POSIX compliant
 * probably this interface will work, if not it same of Windows, if it works ok,
 * else fix it.
 */


int main(){
  full_menu();
  return 0;
}
