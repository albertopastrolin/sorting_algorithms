
/*
 * This is an implementation of the counting sort;
 * to use it you also need this header:
 * define.h
*/

void counting_sort (int in[], int out[], int k);

void counting_sort (int in[], int out[], int k){
  int* work = malloc ((k + 1) * sizeof(int));
  int i;
  array_fill(work, k + 1, 0);
  for (i = 0; i < LENGTH; i += 1)
    work[in[i]] += 1;
  for(i = 1; i <= k; i += 1)
    work[i] += work[i - 1];
  for(i = LENGTH - 1; i >= 0; i -= 1){
    out[work[in[i]]] = in[i];
    work[in[i]] -= 1;
  }
  free(work);
}
