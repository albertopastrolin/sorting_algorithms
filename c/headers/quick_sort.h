

/*
 * This is an implementation of the quick sort;
 * to use it you also need this header:
 * define.h
 * array.h
*/


void quick_sort(int array[], int left, int right);
void naive_quick_sort(int array[], int left, int right);


void quick_sort(int array[], int left, int right){
    if(left < right){
        int pivot = partition (array, left, right);
        quick_sort (array, left, pivot - 1);
        quick_sort (array, pivot + 1, right);
    }
}

void naive_quick_sort(int array[], int left, int right){
  if(left < right){
    int pivot = select_naive(array, left, right, (left + right) / 2);
    quick_sort (array, left, pivot - 1);
    quick_sort (array, pivot + 1, right);
  }
}
