Description of the Various Module
==================================

A quick description of the various module

* array.rb
  In this module you can find various function to manipulate the array
  
* define.rb
  In this module you can find the definition of various macro

* heap.rb
  In this module you can find an implentation of the max heap, and some
  functions to manipulate it
  
* heap_sort.rb
  In this module you can find an implementation of the heap sort algorithm
  
* inserction_sort.rb
  In this module you can find an implementation of the inserction sort algorithm

* merge_sort.rb
  In this module you can find an implementation of the merge sort algorithm

* quick_sort.rb
  In this module you can find an implementation of the quick sort algorithm
  
* counting_sort.rb
  In this module you can find an implementation of the counting sort algorithm
  
* radix_sort.rb
  In this module you can find an implementation of the radix sort algorithm

* bucket_sort.rb
  In this module you can find an implementation of the bucket sort algorithm
