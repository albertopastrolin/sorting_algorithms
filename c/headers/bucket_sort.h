
/*
 * This is an implementation of the bucket sort;
 * to use it you also need this header:
 * define.h
*/

void bucket_sort(int array[]);

void bucket_sort(int array[]){
  int bucket[MAX_RAND + 1] = { 0 }, i, j, k;
  for (i = 0; i < LENGTH; i += 1)
    bucket[array[i]] += 1;
  for (i = 0, j = 0; i <= MAX_RAND; i += 1)
    for (k = bucket[i]; k > 0; k -= 1){
      array[j] = i;
      j += 1;
    }
}
