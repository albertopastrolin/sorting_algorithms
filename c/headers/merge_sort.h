

/*
 * This is an implementation of the merge sort;
 * to use it you also need this header:
 * define.h
 * array.h
*/


void merge(int array[], int left, int center, int right);
void merge_sort(int array[], int left, int right);


void merge_sort (int array[], int left, int right){
    if (left < right){
        int center = (left + right) / 2;
        merge_sort(array, left, center);
        merge_sort(array, center + 1, right);
        merge(array, left, center, right);
    }
}

void merge (int array[], int left, int center, int right){
    int i = left, j = center + 1, k = left, block[LENGTH];
    while (i <= center && j <= right){
        if (array[i] <= array[j]){
            block[k] = array[i];
            i += 1;
        }
        else{
            block[k] = array[j];
            j += 1;
        }
        k += 1;
    }
    while (i <= center) {
        block[k] = array[i];
        i += 1;
        k += 1;
    }
    while (j <= right) {
        block[k] = array[j];
        j += 1;
        k += 1;
    }
    for (k = left; k <= right; k +=1)
        array[k] = block[k];
}
