
/*
 * This is an implementation of the radix sort;
 * to use it you also need this header:
 * define.h
*/

void radix_sort (int in[], int out[]);

void radix_sort (int in[], int out[]){
  int work[BASE], i, digit = 1;
  while(MAX_RAND / digit > 0){
    array_fill(work, BASE, 0);
    for (i = 0; i < LENGTH; i += 1)
      work[(in[i] / digit) % BASE] += 1;
    for(i = 1; i < BASE; i += 1)
      work[i] += work[i - 1];
    for(i = LENGTH - 1; i >= 0; i -= 1){
      out[work[(in[i] / digit) % BASE]] = in[i];
      work[(in[i] / digit) % BASE] -= 1;
    }
    digit *= BASE;
  }
}
