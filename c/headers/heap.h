

/*
 * Various function and implementation of a Max Heap
 * to use it you also need this header:
 * define.h
 * array.h
*/


int heapsize(int heap[]);
void increase(int heap[]);
void decrease(int heap[]);
void setsize(int heap[], int size);
int left(int i);
int right(int i);
void heapify(int heap[], int error);
int extract_max(int heap[]);
void build_heap(int array[]);
void heap_initialize(int heap[]);
void heap_print(int heap[]);


int heapsize(int heap[]){
    return heap[0];
}

void increase(int heap[]){
    heap[0] += 1;
}

void decrease(int heap[]){
    heap[0] -= 1;
}

void setsize(int heap[], int size){
    heap[0] = size;
}

int left(int i){
    return 2 * i;
}

int right(int i){
    return (2 * i) - 1;
}

void heapify(int heap[], int error){
    int m;
    if( left(error) <= heapsize(heap) && heap[left(error)] > heap[error])
        m = left(error);
    else
        m = error;
    if ( right(error) <= heapsize(heap) && heap[right(error)] > heap[m])
        m = right(error);
    if (m != error){
        switcheroo(heap, error, m);
        heapify(heap, m);
    }
}

int extract_max(int heap[]){
    int max = heap[1];
    heap[1] = heap[ heapsize(heap) ];
    decrease(heap);
    heapify(heap, 1);
    return max;
}

void build_heap(int array[]){
    int i;
    setsize(array, LENGTH);
    for (i = LENGTH / 2; i > 0; i -= 1)
        heapify(array, i);
}

void heap_initialize(int heap[]){
    array_initialize(heap);
    switcheroo(heap, 0, LENGTH);
    build_heap(heap);
}

void heap_print(int heap[]){
  int i;
  printf ("Size:\t%d\n", heapsize(heap));
  for(i = 1; i < LENGTH + 1; i += 1)
    if(i % ELEMENTS_FOR_ROW)
      printf ("%8d", heap[i]);
    else
      printf ("%8d\n", heap[i]);
    printf("\n");
}